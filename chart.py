from PyQt5.QtWidgets import QWidget, QFrame, QHBoxLayout, QSplitter, QAction, QListView, QMenu, QLineEdit, QListWidget, QDialog, QListWidgetItem, QTreeView, QTreeWidget, QTreeWidgetItem
from PyQt5.QtCore import Qt, QEvent
from PyQt5 import QtWidgets
# import task


class Chart(QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.tasks = {}

        # Split panel
        hbox = QHBoxLayout(self)
        self.topleft = QTreeWidget(self)
        self.topleft.setHeaderLabel("Tasks")
        self.topleft.setAutoExpandDelay(1)
        self.topleft.setFrameShape(QFrame.StyledPanel)
        self.topleft.resize(30, self.topleft.y())
        self.topleft.setAlternatingRowColors(True)
        self.topleft.setStyleSheet("background-color: rgb(215, 215, 215)")

        self.topright = QFrame(self)
        self.topright.resize(1000, self.topright.y())
        self.topright.setFrameShape(QFrame.StyledPanel)

        self.splitter = QSplitter(Qt.Horizontal)
        self.splitter.addWidget(self.topleft)
        self.splitter.addWidget(self.topright)

        hbox.addWidget(self.splitter)

        # Context Menu events
        self.topleft.installEventFilter(self)
        self.topright.installEventFilter(self)

    def new_task(self):
        """Pop ups a QDialog for naming new task"""
        self.dial = QDialog(self)
        self.line = QLineEdit(self.dial)
        self.line.setFixedSize(300, 25)
        self.line.returnPressed.connect(self.save_new_task)
        self.dial.setWindowTitle("Set name for task")
        self.dial.setFixedSize(300, 50)
        self.dial.show()

    def save_new_task(self):
        """Called when user accepts name of task and saves to QListWidget"""
        task_name = self.line.text()
        new_task = QTreeWidgetItem([task_name])
        self.topleft.addTopLevelItem(new_task)
        # new_task = QListWidgetItem(task_name)
        # self.topleft.addItem(new_task)
        self.dial.close()


    def sub_task(self):
        """Pop ups a QDialog for naming new sub-task"""
        self.sel_task = self.topleft.currentItem()

        if self.sel_task:
            self.dial = QDialog(self)
            self.line = QLineEdit(self.dial)
            self.line.setFixedSize(300, 25)
            self.line.returnPressed.connect(self.save_sub_task)
            self.dial.setWindowTitle("Set name for sub-task")
            self.dial.setFixedSize(300, 50)
            self.dial.show()


    def save_sub_task(self):
        task_name = self.line.text()
        # new_task = QListWidgetItem("    " + task_name)
        new_task = QTreeWidgetItem([task_name])
        new_task.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEditable | Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
        # self.topleft.insertItem(self.sel_task + 1, new_task)
        self.sel_task.addChild(new_task)
        self.topleft.expandItem(self.sel_task)
        self.dial.close()

    def del_task(self):
        root = self.topleft.invisibleRootItem()
        for item in self.topleft.selectedItems():
            (item.parent() or root).removeChild(item)


    def some_action(self):
        print("Some action")

    def assign(self):
        print("Assign")

    def show_props(self):
        print("Show props")


    def eventFilter(self, source, event):
        if (event.type() == QEvent.ContextMenu and source is self.topleft):
            self.left_cmenu(event)
        elif (event.type() == QEvent.ContextMenu and source is self.topright):
            self.right_cmenu(event)

            return True
        return super(Chart, self).eventFilter(source, event)

    def left_cmenu(self, event):
        """
        Pop up context menu at task list
        :param event:
        """
        cmenu = QtWidgets.QMenu()
        cmenu.addAction("New task", self.new_task)
        cmenu.addAction("Create subtask", self.sub_task)
        cmenu.addAction("Delete task", self.del_task)
        cmenu.exec_(self.mapToGlobal(event.pos()))

    def right_cmenu(self, event):
        """
        Pop up context menu at bar list
        :param event:
        """
        cmenu = QtWidgets.QMenu()
        cmenu.addAction("Some action", self.some_action)
        cmenu.addAction("Assign", self.assign)
        cmenu.addAction("Properties", self.show_props)
        cmenu.exec_(self.mapToGlobal(event.pos()))

