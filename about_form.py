from PyQt5.QtWidgets import QDialog, QLabel, QPushButton, QAction
from PyQt5.QtCore import QRect
from PyQt5.QtGui import QFont

class About(QDialog):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi()

    def setupUi(self):
        self.setWindowTitle("About")

        self.resize(400, 300)
        lbl_gpy = QLabel(self)
        lbl_gpy.setText("GanttPy")
        lbl_gpy.setGeometry(QRect(30, 20, 71, 31))
        font = QFont()
        font.setPointSize(12)
        lbl_gpy.setFont(font)

        url_link ="<a href=\"http://www.google.com\">GanttPy website</a>"
        lbl_gpy_web = QLabel(self)
        lbl_gpy_web.setText(url_link)
        lbl_gpy_web.setGeometry(QRect(30, 110, 141, 16))
        lbl_gpy_web.setOpenExternalLinks(True)
        self.btn_close = QPushButton(self)
        self.btn_close.setText("Close")
        self.btn_close.setGeometry(QRect(300, 250, 75, 23))
        self.btn_close.clicked.connect(self.close)
        lbl_creds = QLabel(self)
        lbl_creds.setText("Credits")
        lbl_creds.setGeometry(QRect(30, 190, 47, 13))


