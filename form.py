from PyQt5.QtWidgets import QMainWindow, QWidget, QTabWidget, QStackedWidget, QSplitter, QHBoxLayout, QFrame
from PyQt5.QtCore import Qt
import main_ui
import chart
import about_form


class MainForm(QMainWindow):

    def __init__(self, *args, **kwargs):
        QMainWindow.__init__(self, *args, **kwargs)
        # self.setupUi()
        self.ui = main_ui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.show()

        self.n_tabs = 0

        self.central_widget = QStackedWidget()
        self.setCentralWidget(self.central_widget)

        self.turn_on_signals()

    def new_file(self):
        """
        Create a new tab where it would contain the new Gantt Chart
        """
        tab = chart.Chart()

        if self.n_tabs == 0:
            self.tabs = QTabWidget()
            self.central_widget.addWidget(self.tabs)

        self.n_tabs = self.n_tabs + 1

        # Add tab to tabWidget
        self.tabs.addTab(tab, "Chart " + str(self.n_tabs))

        # self.tabs.

    def open_file(self):
        print("open file")

    def save_file(self):
        print("save file")

    def save_file_as(self):
        print("save file as")


    # Help menu
    def about(self):
        about_dialog = about_form.About(self)
        about_dialog.show()

    def turn_on_signals(self):
        self.ui.actionNew.triggered.connect(self.new_file)
        self.ui.actionOpen.triggered.connect(self.open_file)
        self.ui.actionSave.triggered.connect(self.save_file)
        self.ui.actionSave_As.triggered.connect(self.save_file_as)

        self.ui.actionAbout_GanttPy.triggered.connect(self.about)
