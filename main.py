"""
Software for creating Gantt charts

author: Tadius
copyright:
license: GNU General Public License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

credits:

version: 0.1
maintainer: Tadius
email: smolina699@hotmail.com
status: Beta
"""

import sys
import tango
import form
from PyQt5 import QtWidgets

__author__ = "Tadius"
__license__ = "GNU General Public License"
__version__ = "0.1"
__contact__ = "smolina699@hotmail.com"

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = form.MainForm()
    window.show()
    sys.exit(app.exec_())
